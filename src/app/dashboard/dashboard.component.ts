import { Component, OnInit } from '@angular/core';
import {RepoInfo} from '../repo-info';
import {RepoTypes} from '../app.constants';
import {Router} from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  repoList: RepoInfo[] = [
    {
      name: 'My Public Repo',
      typeOfRepo: RepoTypes.OPEN,
      noOfOwnedCopies: 4,
      allPresentLocally: true
    },
    {
      name: 'My Private Repo',
      typeOfRepo: RepoTypes.OWNED,
      noOfOwnedCopies: 6,
      allPresentLocally: true
    },
    {
      name: 'My Personal Repository',
      typeOfRepo: RepoTypes.OWNED,
      noOfOwnedCopies: 8,
      allPresentLocally: true
    },
    {
      name: 'Miscellaneous',
      typeOfRepo: RepoTypes.OTHER,
      noOfOwnedCopies: 10,
      allPresentLocally: true
    }
  ];
  pageName: string;

  constructor(private router: Router) {
  }

  ngOnInit() {
    this.pageName = this.router.url.split('/')[1];
  }

}
