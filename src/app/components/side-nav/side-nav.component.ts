import { Component, OnInit } from '@angular/core';
import {SharedService} from '../../shared.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.css']
})
export class SideNavComponent implements OnInit {

  navType = 'dashboard';
  constructor(
    private sharedService: SharedService,
    private router: Router
    ) {
  }

  ngOnInit() {
  }

  navigate(navType) {
    this.navType = navType;
    this.router.navigate(['./' + navType]);
  }

}
