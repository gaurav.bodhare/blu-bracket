import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar/navbar.component';
import { SideNavComponent } from './side-nav/side-nav.component';
import {MatButtonModule, MatChipsModule, MatIconModule, MatInputModule, MatSidenavModule, MatTooltipModule} from '@angular/material';

@NgModule({
  declarations: [
    NavbarComponent,
    SideNavComponent
  ],
  exports: [
    NavbarComponent,
    SideNavComponent
  ],
  imports: [
    CommonModule,
    MatIconModule,
    MatChipsModule,
    MatSidenavModule,
    MatButtonModule,
    MatTooltipModule,
    MatInputModule
  ]
})
export class ComponentsModule { }
