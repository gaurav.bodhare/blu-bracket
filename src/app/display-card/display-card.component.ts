import {Component, Input, OnInit} from '@angular/core';
import {RepoInfo} from '../repo-info';

@Component({
  selector: 'app-display-card',
  templateUrl: './display-card.component.html',
  styleUrls: ['./display-card.component.css']
})
export class DisplayCardComponent implements OnInit {

  @Input() repoInfo: RepoInfo;

  constructor() { }

  ngOnInit() {
  }

}
