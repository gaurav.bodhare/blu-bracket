export enum RepoTypes {
    OWNED = 'Enterprise Owned',
    OPEN = 'Open Source',
    OTHER = 'Other',
}
