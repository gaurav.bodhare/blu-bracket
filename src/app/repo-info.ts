import {RepoTypes} from './app.constants';

export class RepoInfo {
  name: string;
  typeOfRepo: RepoTypes;
  noOfOwnedCopies: number;
  allPresentLocally: boolean;
}
